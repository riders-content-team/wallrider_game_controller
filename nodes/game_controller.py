#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from std_srvs.srv import Trigger, TriggerResponse, Empty
from gazebo_msgs.srv import GetModelState
import time
import os
import requests

class GameController:

    def __init__(self, robot_name):

        self.robot_name = robot_name

        self.robot_status = "pause"
        self.game_message = "mission_running"

        self.robot_pose_x = 0
        self.robot_pose_y = 0
        self.robot_sector = "A"

        self.finished = False

        self.metrics = {
            'robot_name' : self.robot_name,
            'robot_status' : self.robot_status,
            'game_message' : self.game_message,
            "sector" : self.robot_sector
        }

        rospy.init_node("game_controller")

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        s = rospy.Service('/robot_handler', Trigger, self.handle_robot)

        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.robot_check = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)

            resp = self.robot_check("wallrider", "")
            if not resp.success == True:
                print("no robot around")
                self.robot_status = "no_robot"
                self.game_message = "mission_stopped"

        except (rospy.ServiceException, rospy.ROSException), e:
            print("no robot around")
            self.robot_status = "no_robot"
            self.game_message = "mission_stopped"

            rospy.logerr("Service call failed: %s" % (e,))

        rate = rospy.Rate(5)


        self.now = rospy.get_time()

        while ~rospy.is_shutdown():
            if not self.robot_status == "no_robot":
                self.check_robot_sector()

            if not self.finished:
                self.now = rospy.get_time()

            self.publish_metrics()
            rate.sleep()

    def handle_robot(self,req):
        if self.robot_status == "pause":
            self.pause_sim()
            self.robot_status = "run"
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "run":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "stop":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )

        elif self.robot_status == "no_robot":
            return TriggerResponse(
                success = True,
                message = self.robot_status
            )
    
    def pause_sim(self):
        try:
            rospy.wait_for_service("/gazebo/pause_physics", 1.0)
            self.pause = rospy.ServiceProxy("/gazebo/pause_physics", Empty)

            resp = self.pause()
        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def publish_metrics(self):

        self.metrics['status'] = str(self.robot_status)
        self.metrics['robot_status'] = str(self.robot_status)
        self.metrics['sector'] = str(self.robot_sector)
        self.metrics['time'] = str(self.now)

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))

    def check_robot_sector(self):
        resp = self.robot_check("wallrider", "")
        self.robot_pose_x = resp.pose.position.x
        self.robot_pose_y = resp.pose.position.y

        # first straight 
        if self.robot_pose_y > 11 and self.robot_sector == "A":
            self.robot_sector = "B"
            #self.achieve()
        
        # 1st turn
        if self.robot_pose_x < -11 and self.robot_sector == "B":
            self.robot_sector = "C"
            #self.achieve()

        # 2nd turn
        if self.robot_pose_y > 30 and self.robot_sector == "C":
            self.robot_sector = "D"
            #self.achieve()

        # 3rd turn
        if self.robot_pose_y < 33 and self.robot_sector == "D":
            self.robot_sector = "E"
            #self.achieve()

        # restricted area
        if self.robot_pose_x < -49 and self.robot_sector == "E":
            self.robot_sector = "F"
            #self.achieve()

        # 1st straight after restricted area
        if self.robot_pose_x < -64 and self.robot_sector == "F":
            self.robot_sector = "G"
            #self.achieve()

        # 2nd straight after restricted area
        if self.robot_pose_x < -82 and self.robot_sector == "G":
            self.robot_sector = "H"
            #self.achieve()

        # 3rd straight after restricted area
        if self.robot_pose_x < -100 and self.robot_sector == "H":
            self.robot_sector = "I"
            #self.achieve()
            self.finished = True

    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })


if __name__ == '__main__':
    # Name of robot
    controller = GameController("wallrider")
